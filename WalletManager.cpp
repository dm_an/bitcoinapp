#include <cstdlib>
#include <cstring>
#include "WalletManager.h"

WalletManager::WalletManager() {
    wallets = new Wallet*[GLOBAL_SIZE_INCR];
    capacity = GLOBAL_SIZE_INCR;            
    count = 0;

    coins = new BitCoin*[GLOBAL_SIZE_INCR];
    coinsCapacity = GLOBAL_SIZE_INCR;            
    coinCount = 0;
}

WalletManager::~WalletManager() {
    // delete all the wallets
    for (int i = 0; i < count; i++)
        delete wallets[i];
    
    delete [] wallets;

    delete [] coins;
}

bool WalletManager::addBitCoinToWallet(char* id, char* coinId)
{
    Wallet* targetWallet = NULL;
    // search in all the wallets if bitcoin already exists
    for (int i = 0; i < count; i++)
    {
        if (wallets[i]->bitCoinExists(coinId)) return false;
        if (strcmp(wallets[i]->getID(), id) == 0) targetWallet = wallets[i];
    }
    
    if (targetWallet == NULL) return false;
    BitCoin* coin = targetWallet->addBitCoin(coinId);
    
    addBitCoin(coin);
    return true;
}

bool WalletManager::addWallet(char* id)
{
    // check if wallet id already exists.
    for (int i = 0; i < count; i++)
    {
        if (strcmp(wallets[i]->getID(), id) == 0) return false;
    }
    
    if (count == capacity)
    {
        // limit reached, increase the size of the array.
        Wallet** newWallets = new Wallet*[capacity + GLOBAL_SIZE_INCR];
        for (int i = 0; i < count; i++)
        {
            newWallets[i] = wallets[i];
        }
        capacity += GLOBAL_SIZE_INCR;
        delete [] wallets;
        wallets = newWallets;
    }
    
    Wallet* wallet = new Wallet(id);
    wallets[count] = wallet;
    count++;
    return true;
}

void WalletManager::addBitCoin(BitCoin* coin)
{
    if (coinCount == coinsCapacity)
    {
        // limit reached, increase the size of the array.
        BitCoin** newCoins = new BitCoin*[coinsCapacity + GLOBAL_SIZE_INCR];
        for (int i = 0; i < coinCount; i++)
        {
            newCoins[i] = coins[i];
        }
        coinsCapacity += GLOBAL_SIZE_INCR;
        delete [] coins;
        coins = newCoins;
    }
    
    coins[coinCount] = coin;
    coinCount++;
}

bool WalletManager::isViableTran(const char* id, int tranAmount)
{
    Wallet* targetWallet = NULL;
    // search in all the wallets if bitcoin already exists
    for (int i = 0; i < count; i++)
    {
        if (strcmp(wallets[i]->getID(), id) == 0)
        {
            targetWallet = wallets[i];
            break;
        }
    }
    
    if(targetWallet == NULL || targetWallet->getBalanceAmount() < tranAmount) return false;
    return true;    
}


void WalletManager::executeTransaction(const char* tId, const char* sId, const char* rId, int amount, int year, const char* time, Wallet** senderWallet, Wallet** recWallet)
{
    *senderWallet = NULL;
    for (int i = 0; i < count; i++)
    {
        if (strcmp(wallets[i]->getID(), sId) == 0)
        {
            *senderWallet = wallets[i];
            break;
        }
    }
    if (*senderWallet != NULL)
    {
        Tran* trans = (*senderWallet)->sendMoney(tId, rId, amount, year, time);
        // update the receive wallet for the list of transactions;
        
        *recWallet = NULL;
        for (int i = 0; i < count; i++)
        {
            if (strcmp(wallets[i]->getID(), rId) == 0)
            {
                *recWallet = wallets[i];
                break;
            }
        }
        
        if(*recWallet != NULL)
        {
            (*recWallet)->receiveMoney(trans);
        }
    }
}

Wallet* WalletManager::walletExists(char* id)
{
    for (int i = 0; i < count; i++)
    {
        if (strcmp(wallets[i]->getID(), id) == 0)
        {
            return wallets[i];;
        }
    }
    
    return NULL;
}

BitCoin* WalletManager::coinExists(char* id)
{
    for (int i = 0; i < coinCount; i++)
    {
        if (strcmp(coins[i]->getId(), id) == 0)
        {
            return coins[i];;
        }
    }
    
    return NULL;
}

/*
Tran* WalletManager::findEarnings(char* id)
{
    Wallet* recWallet = walletExists(id);
    if (recWallet != NULL)
    {
        return recWallet->getReceiveTrans();
    }
}

Tran* WalletManager::findPayments(char* id)
{
    Wallet* sendWallet = walletExists(id);
    if (sendWallet != NULL)
    {
        return sendWallet->getSendTrans();
    }
}
*/