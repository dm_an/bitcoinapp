#ifndef WALLET_H
#define WALLET_H

#include "BitCoin.h"

typedef struct TransListNode
{
    Tran* tran;
    struct TransListNode* next;
} TransNode ;

class Wallet {
private:
    char* walletId;
    BitCoin** coins;
    int count;
    int capacity;
    int walletAmount;
    TransNode* sendListHead;
    TransNode* sendListLast;
    TransNode* recListHead;
    TransNode* recListLast;
public:
    //Wallet();
    Wallet(char* id);
    ~Wallet();
    char* getID();
    int getBalanceAmount();
    BitCoin* addBitCoin(char* id);
    BitCoin* bitCoinExists(char* id);
    Tran* sendMoney(const char* tId, const char* rId, int amount, int year, const char* time);
    void receiveMoney(Tran* trans);
    int getCoinCount();
    TransNode* getReceiveTrans();
    TransNode* getSendTrans();
};

#endif /* WALLET_H */

