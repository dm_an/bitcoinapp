#ifndef HASHMANAGER_H
#define HASHMANAGER_H

#include "HashTableEntry.h"

class HashManager {
private:
    HashTableEntry* sendTable;
    int sendNums;    
    HashTableEntry* recTable;
    int recNums;
public:
    HashManager();
    ~HashManager();
    void updateTables(Wallet* sendWallet, Wallet* recWallet);
    TransNode* findEarnings(char* walletId);
    TransNode* findPayments(char* walletId);
    
private:
    int getHashIndex(char* walletId, int numEntries);
};

#endif /* HASHMANAGER_H */

