#ifndef WALLETMANAGER_H
#define WALLETMANAGER_H

#include "Wallet.h"

class WalletManager {
private:
    Wallet** wallets;
    BitCoin** coins;
    int count;
    int capacity;
    int coinCount;
    int coinsCapacity;
public:
    WalletManager();
    ~WalletManager();
    
    bool addWallet(char* id);
    Wallet* walletExists(char* id);
    BitCoin* coinExists(char* id);
    bool addBitCoinToWallet(char* id, char* coinId);
    bool isViableTran(const char *id, int tranAmount);
    void executeTransaction(const char* tId, const char* sId, const char* rId, int amount, int year, const char* time, Wallet** senderWallet, Wallet** recWallet);
    //Tran* findEarnings(char* id);
    //Tran* findPayments(char* id);
    
private:
    void addBitCoin(BitCoin* coin);
};

#endif /* WALLETMANAGER_H */

