#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstring>
#include <string>
#include "BitCoinAppWorker.h"

using namespace std;

/**
 * Constructor
 */
BitCoinAppWorker::BitCoinAppWorker() {
    lastTranTime = "";
}

/**
 * Destructor
 */
BitCoinAppWorker::~BitCoinAppWorker() {
}

/**
 * Reads the bitcoin balance file to start with the initial balances in the bitcoins
 * It checks for the duplicate bitcoins and duplicate user ids as well and
 * shows the appropriate error to the user
 * @param bitCoinFile the input file
 * @return true if success else false
 */
bool BitCoinAppWorker::readBitCoinFile(const char* bitCoinFile)
{
    ifstream inFile(bitCoinFile);
    if (!inFile.good()) return false;

    bool isError = false;
    string line;
    int lineCount = 0;
    while(getline(inFile, line))
    {
        lineCount++;
	int sizeL = line.size();
	if (line[sizeL-1] == '\r')
		line = line.substr(0, sizeL-1);

        //cout << line << endl;
        istringstream iss(line);
        string tok;
        bool isWalletID = false;
        char* walletId;
        while (getline(iss, tok, ' '))
        {
            if (!isWalletID)
            {
                // this is the wallet id
                walletId = new char[tok.size() + 1];
                strcpy(walletId, tok.c_str());
                if (!walletsWorker.addWallet(walletId))
                {
                    isError = true;
                    cout << "Error reading bitCoinBalance file. Line# " << lineCount << ": User already exists." << endl;
                    delete walletId;
                    break;
                }
                isWalletID = true;
            }
            else
            {
                // this is a bitcoin id
                char* coinId = new char[tok.size() + 1];
                strcpy(coinId, tok.c_str());
                if (!walletsWorker.addBitCoinToWallet(walletId, coinId))
                {
                    isError = true;
                    cout << "Error reading bitCoinBalance file. Line# " << lineCount << ": Duplicate BitCoinID found." << endl;
                    delete coinId;
                    break;
                }
            }
        }

        if (isError) break;
    }

    inFile.close();
    return !isError;
}

bool BitCoinAppWorker::readTransFile(const char* tranFile)
{
    ifstream inFile(tranFile);
    if (!inFile.good()) return false;

    string line;
    int lineCount = 0;
    while(getline(inFile, line))
    {
        lineCount++;
	int sizeL = line.size();
	if (line[sizeL-1] == '\r')
		line = line.substr(0, sizeL-1);
        //cout << line << endl;
        stringstream iss(line);
        string tranId, sender, receiver, date, time;
        int amount;

        iss >> tranId >> sender >> receiver >> amount >> date >> time;
        //cout << "Read = " << tranId << "-" << sender << '-' << receiver << "-" << amount << "-" << date << "-" << time << endl;
        // if transaction is not possible, continue to read next transaction details
        if(sender.compare(receiver) == 0) continue;
        if (!walletsWorker.isViableTran(sender.c_str(), amount)) continue;
        executeTransaction(line);
    }

    inFile.close();
    return true;
}

void BitCoinAppWorker::executeTransaction(std::string tranLine)
{
    stringstream iss(tranLine);
    string tranId, sender, receiver, date, time;
    int amount;
    iss >> tranId >> sender >> receiver >> amount >> date >> time;
    stringstream yS(date.substr(6));
    int year;
    yS >> year;

    Wallet* senderWallet;
    Wallet* recWallet;
    walletsWorker.executeTransaction(tranId.c_str(), sender.c_str(), receiver.c_str(), amount, year, time.c_str(), &senderWallet, &recWallet);
    updateLastTransactionTime(date, time);

    // push sender Walletid in the senderhashtable and receive wallet id in receiver hash table
    tablesWorker.updateTables(senderWallet, recWallet);
}

void BitCoinAppWorker::updateLastTransactionTime(string date, string time)
{
    lastTranTime = date + ":" + time;
}

bool BitCoinAppWorker::executeCommand(string cmdline)
{
    char** args = NULL;
    int argCount = 0;
    bool retStatus = false;

    cout << endl;

    getArguments(cmdline, &args, &argCount);
    if (argCount > 5 || args == NULL)
    {
        showUnsupportCmd();
    }
    else
    {
        if ((strcmp(args[0], "exit") == 0) && argCount == 0) retStatus = true;
        else if ((strcmp(args[0], "requestTransaction") == 0) && argCount == 5) runTransReq(args);
        else if ((strcmp(args[0], "requestTransactions") == 0) && argCount == 1) runTransReqFile(args[1]);
        else if ((strcmp(args[0], "findEarnings") == 0) && (argCount % 2) == 1) runFindEarnings(args, argCount);
        else if ((strcmp(args[0], "findPayments") == 0) && (argCount % 2) == 1) runFindPayments(args, argCount);
        else if ((strcmp(args[0], "walletStatus") == 0) && argCount == 1) runWalletStatus(args[1]);
        else if ((strcmp(args[0], "bitCoinStatus") == 0) && argCount == 1) runBitCoinStatus(args[1]);
        else if ((strcmp(args[0], "traceCoin") == 0) && argCount == 1) runTraceCoin(args[1]);
        else showUnsupportCmd();

        for (int i = 0; i <= argCount; i++)
            delete args[i];
    }
    cout << endl;
    return retStatus;
}

void BitCoinAppWorker::runTransReqFile(char* arg)
{
    ifstream inFile(arg);
    if (!inFile.good())
    {
        cout << "The transactions file " << arg << " could not be opened." << endl;
        return;
    }

    string line;
    int lineCount = 0;
    while(getline(inFile, line))
    {
        lineCount++;
        int sizeL = line.size();
        line = line.substr(0, sizeL-1);
        //cout << line << endl;
        stringstream iss(line);
        string sender, receiver, date, time;
        string tranId = "INT";
        int amount;

        iss >> sender >> receiver >> amount >> date >> time;
        //cout << "Read = " << tranId << "-" << sender << '-' << receiver << "-" << amount << "-" << date << "-" << time << endl;
        // if transaction is not possible, continue to read next transaction details
        if(sender.compare(receiver) == 0) continue;
        if (!walletsWorker.isViableTran(sender.c_str(), amount)) continue;
        string tranTime = date + ":" + time;
        if (tranTime.compare(lastTranTime) <= 0) continue;

        string cmd = "INT " + line;
        executeTransaction(cmd);
    }

    inFile.close();
    cout << "The transactions Input file has been processed successfully." << endl;
}

void BitCoinAppWorker::showUnsupportCmd()
{
    cout << "Unsupported command / parameters. Please try again." << endl;
}

void BitCoinAppWorker::getArguments(std::string cmd, char*** args, int* argCount)
{
    istringstream iss(cmd);
    int largCount = -1;
    string tok;

    while(getline(iss, tok, ' '))
    {
        if (tok.empty()) continue;
        largCount++;
    }

    *argCount = largCount;
    if (largCount > 5) return;

    *args = new char*[largCount];
    istringstream rss(cmd);
    largCount = -1;
    while(getline(rss, tok, ' '))
    {
        if (tok.empty()) continue;
        largCount++;
        (*args)[largCount] = new char[tok.size() + 1];
        strcpy((*args)[largCount], tok.c_str());
    }
}

void BitCoinAppWorker::runTransReq(char** args)
{
    if (walletsWorker.walletExists(args[1]) == NULL)
    {
        cout << "Sender Wallet ID does not exist." << endl;
        return;
    }
    if (walletsWorker.walletExists(args[2]) == NULL)
    {
        cout << "Sender Wallet ID does not exist." << endl;
        return;
    }
    if (!walletsWorker.isViableTran(args[1], atoi(args[3])))
    {
        cout << "Sender does not have sufficient balance." << endl;
        return;
    }
    string date = args[4];
    string time = args[5];
    string tranTime = date + ":" + time;
    if (tranTime.compare(lastTranTime) <= 0)
    {
        cout << "Can not execute a transaction past in time." << endl;
        return;
    }

    //string cmd = "INT " + args[1] + " " + args[2] + " " + args[3] + " " + args[4] + " " + args[5];
    ostringstream oss;
    oss << "INT " << args[1] << " " << args[2] << " " << args[3] << " " << args[4] << " " << args[5];
    executeTransaction(oss.str());
    cout << "The requested transaction has been processed." << endl;
}

void BitCoinAppWorker::runFindEarnings(char** args, int argc)
{
    if (checkArgsValid(args, argc))
    {
        // do stuff
        int syear = -1;
        int eyear = -1;
        char* stime = NULL;
        char* etime = NULL;
        if (argc == 3)
        {
            stime = args[2];
            etime = args[3];
        }
        else if (argc == 5)
        {
            stime = args[2];
            etime = args[4];
            syear = atoi(args[3]);
            eyear = atoi(args[5]);
        }

        TransNode* earnTrans = tablesWorker.findEarnings(args[1]);
	if (earnTrans == NULL)
	{
		cout << "No transaction found for the given criteria." << endl;
	}
        else displayTranList(earnTrans, stime, etime, syear, eyear, 1);
    }
    else
    {
        cout << "Command- findEarnings CAN NOT BE EXECUTED. INVALID ARGUMENTS" << endl;
    }
}

void BitCoinAppWorker::runFindPayments(char** args, int argc)
{
    if (checkArgsValid(args, argc))
    {
        // do stuff
        int syear = -1;
        int eyear = -1;
        char* stime = NULL;
        char* etime = NULL;
        if (argc == 3)
        {
            stime = args[2];
            etime = args[3];
        }
        else if (argc == 5)
        {
            stime = args[2];
            etime = args[4];
            syear = atoi(args[3]);
            eyear = atoi(args[5]);
        }

        TransNode* payTrans = tablesWorker.findPayments(args[1]);
        if (payTrans == NULL)
	{
		cout << "No transaction found for the given criteria." << endl;
	}
        else displayTranList(payTrans, stime, etime, syear, eyear, 0);
    }
    else
    {
        cout << "Command- findPayments CAN NOT BE EXECUTED. INVALID ARGUMENTS" << endl;
    }
}

void BitCoinAppWorker::runWalletStatus(char* arg)
{
    Wallet* wallet = walletsWorker.walletExists(arg);
    if (wallet != NULL)
    {
        int walletAmount = wallet->getBalanceAmount();
        cout << "The wallet amount in the wallet '" << arg << "' is " << walletAmount << endl;
    }
    else
    {
        cout << "The wallet with id '" << arg << "' COULD NOT BE LOCATED." << endl;
    }
}

void BitCoinAppWorker::runBitCoinStatus(char* arg)
{
    BitCoin* coin = walletsWorker.coinExists(arg);
    if (coin != NULL)
    {
        int coinAmount = coin->getUnspentAmount();
        cout << "The unspent amount in the BitCoin '" << arg << "' is " << coinAmount << endl;
        cout << coin->getId() << " " << coin->getTransCount() << " " << coin->getUnspentAmount() << endl;
    }
    else
    {
        cout << "The BitCoin with id '" << arg << "' COULD NOT BE LOCATED." << endl;
    }
}

void BitCoinAppWorker::runTraceCoin(char* arg)
{
    BitCoin* coin = walletsWorker.coinExists(arg);
    if (coin != NULL)
    {
        Tran* tree = coin->getTranTree();
        displayTranTree(tree);
    }
    else
    {
        cout << "The BitCoin with id '" << arg << "' COULD NOT BE LOCATED." << endl;
    }
}

bool BitCoinAppWorker::checkArgsValid(char** args, int argc)
{
    bool validArgs = true;
    if (argc == 3)
    {
        if (!isValidTime(args[2]) || !isValidTime(args[3]))
        {
            validArgs = false;
        }
    }
    else if (argc == 5)
    {
        if (!isValidTime(args[2]) || !isValidTime(args[4]) || !isValidYear(args[3]) || !isValidYear(args[5]))
        {
            validArgs = false;
        }
    }

    return validArgs;
}

bool BitCoinAppWorker::isValidTime(char* arg)
{
    if (strlen(arg) == 5 &&
            arg[2] == ':' &&
            isDigitInRange(arg[0], 0, 2) &&
            ((arg[0] == '2' && isDigitInRange(arg[1], 0, 3)) || (isDigitInRange(arg[1], 0, 9))) &&
            isDigitInRange(arg[3], 0, 5) &&
            isDigitInRange(arg[4], 0, 9))
        return true;

    return false;
}

bool BitCoinAppWorker::isValidYear(char* arg)
{
    if (strlen(arg) == 4 &&
            isDigitInRange(arg[0], 1, 9) &&
            isDigitInRange(arg[1], 0, 9) &&
            isDigitInRange(arg[2], 0, 9) &&
            isDigitInRange(arg[3], 0, 9))
        return true;

    return false;
}

bool BitCoinAppWorker::isDigitInRange(char ch, int min, int max)
{
    char minC = '0' + min;
    char maxC = '0' + max;
    if (ch >= minC && ch <= maxC) return true;
    return false;
}

void BitCoinAppWorker::displayTranList(TransNode* head, char* stime, char* etime, int syear, int eyear, int sendRec)
{
    TransNode* temp = head;
    while (temp != NULL)
    {
        if (stime != NULL &&
                (strcmp(temp->tran->time, stime) < 0 || strcmp(temp->tran->time, etime) > 0))
        {
            temp = temp->next;
            continue;
        }
        else if (syear != -1 && (temp->tran->year < syear || temp->tran->year > eyear))
        {
            temp = temp->next;
            continue;
        }

        if(sendRec == 1)
            cout << "Received " << temp->tran->amount << " from " << temp->tran->fromWalletID << endl;
        else
            cout << "Sent " << temp->tran->amount << " to " << temp->tran->toWalletID << endl;

        temp = temp->next;
    }
}

void BitCoinAppWorker::displayTranTree(Tran* root)
{
    PrintNode *rootNode = new PrintNode();
    rootNode->tran = root;
    rootNode->next = NULL;

    PrintNode *frontNode = rootNode;
    PrintNode *rearNode = rootNode;
    int count = 0;

    while(frontNode != NULL)
    {
        count++;
        if (frontNode->tran->left != NULL)
        {
            PrintNode *newNode = new PrintNode();
            newNode->tran = frontNode->tran->left;
            newNode->next = NULL;
            rearNode->next = newNode;
            rearNode = newNode;
        }
        if (frontNode->tran->right != NULL)
        {
            PrintNode *newNode = new PrintNode();
            newNode->tran = frontNode->tran->right;
            newNode->next = NULL;
            rearNode->next = newNode;
            rearNode = newNode;
        }

        cout << "Tran #" << count << ": Name: " << frontNode->tran->ownerWalletID << ", Amount: " << frontNode->tran->amount << endl;
        frontNode = frontNode->next;
    }

    // free all the nodes.
    frontNode = rootNode;
    while(frontNode != NULL)
    {
        PrintNode *tempNode = frontNode;
        frontNode = frontNode->next;
        delete tempNode;
    }
}
