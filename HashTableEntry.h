#ifndef HASHTABLEENTRY_H
#define HASHTABLEENTRY_H

#include "Wallet.h"

typedef struct BucketNode
{
    Wallet** wallets;
    int count;
    struct BucketNode* next;
} BucketNode;


class HashTableEntry {
private:
    BucketNode *head;
    BucketNode *last;
    
public:
    HashTableEntry();
    ~HashTableEntry();
    
    void pushWallet(Wallet* wallet);
    Wallet* getWallet(char* id);
private:

};

#endif /* HASHTABLEENTRY_H */

