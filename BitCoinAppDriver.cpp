#include <cstdlib>
#include <iostream>
#include <cstring>
#include "CommonStructs.h"
#include "BitCoinAppWorker.h"

#define ARGS_TAKEN 1

using namespace std;

int gBitCoinValue;
int gBucketSize;
const int GLOBAL_SIZE_INCR = 5;
int gBucketCapacity;
int gSendTableNums;
int gRecTableNums;

/*
 * 
 */
int main(int argc, char** argv) {

#if ARGS_TAKEN
    if (argc != 13 ||
            (strcmp(argv[1], "-a") != 0) ||
            (strcmp(argv[3], "-t") != 0) ||
            (strcmp(argv[5], "-v") != 0) ||
            (strcmp(argv[7], "-h1") != 0) ||
            (strcmp(argv[9], "-h2") != 0) ||
            (strcmp(argv[11], "-b") != 0))
    {
        cout << "Invalid command arguments." << endl;
        return -1;
    }
    
    gBitCoinValue = atoi(argv[6]);
    gSendTableNums = atoi(argv[8]);
    gRecTableNums = atoi(argv[10]);
    gBucketSize = atoi(argv[12]);

#else    
    gBitCoinValue = 50;
    gBucketSize = 1024;
    gSendTableNums = 10;
    gRecTableNums = 10;
#endif
    
    // calculate the bucket capacity.
    int sizeLeft = gBucketSize - sizeof(int) - sizeof(BucketNode*);
    gBucketCapacity = sizeLeft / sizeof(Wallet*);

#if ARGS_TAKEN    
    char* balanceFile = argv[2];
    char* tranFile = argv[4];
#else
    char* balanceFile = "bitCoinBalancesFile.txt";
    char* tranFile = "transactionsFile.txt";    
#endif
    
    BitCoinAppWorker appWorker;    

    if(!appWorker.readBitCoinFile(balanceFile)) return -1;
    cout << "The BitCoin Balance file has been processed successfully." << endl << endl;
    if(!appWorker.readTransFile(tranFile)) return -1;
    cout << "The Transactions file has been processed successfully." << endl << endl;
    
    while(true)
    {
        string cmdLine;
        cout << "-> ";
        getline(cin, cmdLine);
        if (appWorker.executeCommand(cmdLine)) break;
    }
    return 0;
}

