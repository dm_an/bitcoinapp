#ifndef BITCOINAPPWORKER_H
#define BITCOINAPPWORKER_H

#include "HashManager.h"
#include "WalletManager.h"

typedef struct PrintTreeNode
{
    Tran* tran;
    PrintTreeNode* next;
} PrintNode ;

class BitCoinAppWorker {
private:
    HashManager tablesWorker;
    WalletManager walletsWorker;
    std::string lastTranTime;
    
public:
    BitCoinAppWorker();
    ~BitCoinAppWorker();
    
    bool readBitCoinFile(const char *inFile);
    bool readTransFile(const char *inFile);
    bool executeCommand(std::string cmdline);
    
private:
    void executeTransaction(std::string tranLine);
    void updateLastTransactionTime(std::string date, std::string time);
    void runTransReq(char** args);
    void runTransReqFile(char* arg);
    void runTraceCoin(char* arg);
    void runFindEarnings(char** args, int argC);
    void runFindPayments(char** args, int arC);
    void runWalletStatus(char* arg);
    void runBitCoinStatus(char* arg);
    void getArguments(std::string cmd, char*** args, int* argCount);
    void showUnsupportCmd();
    bool checkArgsValid(char** args, int argc);
    bool isValidTime(char* arg);
    bool isValidYear(char* arg);
    bool isDigitInRange(char ch, int min, int max);
    void displayTranList(TransNode* head, char* stime, char* etime, int syear, int eyear, int sendRec);
    void displayTranTree(Tran* root);
};

#endif /* BITCOINAPPWORKER_H */

