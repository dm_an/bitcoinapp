#include <cstdlib>
#include <cstring>
#include "HashTableEntry.h"

HashTableEntry::HashTableEntry() {
    head = NULL;
    last = NULL;
}

HashTableEntry::~HashTableEntry() {
    // we just need to deallocate the wallets array
    BucketNode *tNode = head;
    while(tNode != NULL)
    {
        delete [] tNode->wallets;
        BucketNode *delNode = tNode;
        tNode = tNode->next;
        delete delNode;
    }
}

void HashTableEntry::pushWallet(Wallet* wallet)
{
    bool found = false;
    BucketNode* tNode = head;
    while(tNode != NULL)
    {
        for (int i = 0; i < tNode->count; i++)
        {
            if (tNode->wallets[i] == wallet)
            {
                found = true;
                break;
            }
        }
        if (found) break;
        else tNode = tNode->next;
    }
    
    if(!found)
    {
        // we are here, means,we have to add the wallet id in the table
        if (last != NULL && last->count < gBucketCapacity)
        {
            last->wallets[last->count] = wallet;
            last->count++;
        }
        else
        {
            //make a new bucket.
            BucketNode *newBucket = new BucketNode();
            newBucket->wallets = new Wallet*[gBucketCapacity];
            newBucket->wallets[0] = wallet;
            newBucket->count = 1;
            newBucket->next = NULL;
            
            if (last == NULL) head = newBucket;
            else last->next = newBucket;
            last = newBucket;
        }
    }
}

Wallet* HashTableEntry::getWallet(char* id)
{
    BucketNode* tNode = head;
    while(tNode != NULL)
    {
        for (int i = 0; i < tNode->count; i++)
        {
            if (strcmp(tNode->wallets[i]->getID(), id) == 0)
            {
                return tNode->wallets[i];
            }
        }
	tNode = tNode->next;
    }
    
    return NULL;
}
