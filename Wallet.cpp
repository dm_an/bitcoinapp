#include <cstdlib>
#include <cstring>
#include "Wallet.h"

//Wallet::Wallet() {}

Wallet::Wallet(char* id) {
    walletId = id;
    count = 0;
    coins = new BitCoin*[GLOBAL_SIZE_INCR];
    capacity = GLOBAL_SIZE_INCR;
    walletAmount = 0;
    
    sendListHead = sendListLast = recListHead = recListLast = NULL;
}

Wallet::~Wallet() {
    // we just need to free the bitcoins here.
    for (int i = 0; i < count; i++)
        delete coins[i];
    
    delete [] coins;
    delete walletId;
    
    TransNode* tempNode = sendListHead;
    while(tempNode != NULL)
    {
        TransNode* deleteNode = tempNode;
        tempNode = tempNode->next;
        delete deleteNode;
    }
    
    tempNode = recListHead;
    while(tempNode != NULL)
    {
        TransNode* deleteNode = tempNode;
        tempNode = tempNode->next;
        delete deleteNode;
    }
    //since all the transactions are part of the bitcoins trees, they will get deallocated as bitcoins
}

char* Wallet::getID() { return walletId; }
int Wallet::getBalanceAmount() { return walletAmount; }

BitCoin*  Wallet::addBitCoin(char* id)
{
    if (count == capacity)
    {
        // limit reached, increase the size of the array.
        BitCoin** newCoins = new BitCoin*[capacity + GLOBAL_SIZE_INCR];
        for (int i = 0; i < count; i++)
        {
            newCoins[i] = coins[i];
        }
        capacity += GLOBAL_SIZE_INCR;
        delete [] coins;
        coins = newCoins;
    }
    
    BitCoin *coin = new BitCoin(id, walletId);
    coins[count] = coin;
    count++;
    walletAmount += gBitCoinValue;
    return coin;
}

BitCoin* Wallet::bitCoinExists(char* id)
{
    for (int i = 0; i < count; i++)
    {
        if (strcmp(coins[i]->getId(), id) == 0) return coins[i];
    }
    return NULL;
}

int Wallet::getCoinCount() { return count; }

Tran* Wallet::sendMoney(const char* tId, const char* rId, int amount, int year, const char* time)
{
    int sendAmount = amount;
    Tran* retList = NULL;
    while (sendAmount > 0)
    {
        // find maximum of the value for each bit coin in the wallet.
        int max = 0;
        BitCoin* sendCoin = NULL;
        for (int i = 0; i < count; i++)
        {
            if (coins[i]->balanceAmount() > max)
            {
                sendCoin = coins[i];
                max = coins[i]->balanceAmount();
            }
        }
        
        // We have the coin with the highest amount left.
        // Transfer from this bitcoin.
        int coinBalanceAmount = sendCoin->balanceAmount();
        Tran* tran = new Tran();
        tran->tranId = new char[strlen(tId) + 1];
        strcpy(tran->tranId, tId);
        tran->coinId = new char[strlen(sendCoin->getId())+1];
        strcpy(tran->coinId, sendCoin->getId());
        tran->ownerWalletID = new char[strlen(rId) + 1];
        strcpy(tran->ownerWalletID, rId);
        tran->toWalletID = tran->ownerWalletID;
        tran->fromWalletID = walletId;
        tran->year = year;
        tran->time = (char*)time;
        tran->next = NULL;
        tran->left = NULL;
        tran->right = NULL;
        
        if(sendAmount <= coinBalanceAmount)
        {
            tran->amount = sendAmount;
            coinBalanceAmount -= sendAmount;
            sendAmount = 0;
        }
        else
        {
            tran->amount = coinBalanceAmount;
            coinBalanceAmount = 0;
            sendAmount -= coinBalanceAmount;
        }
        
        // create a new trans node
        TransNode *newNode = new TransNode();
        newNode->tran = tran;
        newNode->next = NULL;
        
        if(sendListHead == NULL) sendListHead = newNode;
        else sendListLast->next = newNode;
        sendListLast = newNode;
        
        sendCoin->registerTran(coinBalanceAmount, tran);
        if(retList == NULL) retList = tran;
    }
    
    walletAmount -= amount;
    
    return retList;
}

void Wallet::receiveMoney(Tran* trans)
{
    Tran* tran = trans;
    while(tran != NULL)
    {
        BitCoin* coin = bitCoinExists(tran->coinId);        
        if (coin != NULL)
        {
            coin->addAmount(tran->amount);
        }
        else
        {
            coin = addBitCoin(tran->coinId);
            coin->updateAmount(tran->amount);
        }
        
        walletAmount += tran->amount;
        tran = tran->next;
    }
    // create a new trans node
    TransNode *newNode = new TransNode();
    newNode->tran = trans;
    newNode->next = NULL;

    if(recListHead == NULL) recListHead = newNode;
    else recListLast->next = newNode;
    recListLast = newNode;
}

TransNode* Wallet::getReceiveTrans() { return recListHead; }
TransNode* Wallet::getSendTrans() { return sendListHead; }

