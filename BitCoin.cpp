#include <cstring>
#include <cstdlib>
#include "BitCoin.h"

//BitCoin::BitCoin() {}

BitCoin::BitCoin(char* coinId, char* walletId) {
    id = coinId;
    //ownerId = new char[strlen(walletId)];
    
    //strcpy(id, coinId);
    ownerId = walletId;
    //strcpy(ownerId, walletId);
    amount = unspentAmount = gBitCoinValue;
    
    tranRoot = new Tran();
    tranRoot->coinId = NULL;
    tranRoot->amount = amount;
    tranRoot->ownerWalletID = walletId;
    tranRoot->left = NULL;
    tranRoot->right = NULL;
    tranRoot->toWalletID = NULL;
    tranRoot->fromWalletID = NULL;
    tranRoot->next = NULL;
    tranCount = 0;
    
    currentNode = tranRoot;
}


BitCoin::~BitCoin() {
    // we have to free the memory
    // first delete the tree
    deleteTransTree(tranRoot);    
    delete id;
}

char* BitCoin::getId() { return id; }
int BitCoin::balanceAmount() { return amount; }
void BitCoin::updateAmount(int newamount)
{
    amount = newamount;
    tranRoot->amount = newamount;
}

void BitCoin::addAmount(int newamount) { amount += newamount; }

void BitCoin::registerTran(int balanceAmount, Tran* tran)
{
    amount = unspentAmount = balanceAmount;
    tranCount++;
    currentNode->left = tran;
    if (amount > 0)
    {
        Tran* bNode = new Tran();
        bNode->amount = amount;
        bNode->ownerWalletID = ownerId;
        bNode->left = bNode->right = NULL;
        bNode->next = NULL;
        bNode->toWalletID = NULL;
        bNode->fromWalletID = NULL;
        
        currentNode->right = bNode;
        currentNode = bNode;
    }    
}

int BitCoin::getUnspentAmount() { return unspentAmount; }
int BitCoin::getTransCount() { return tranCount; }

Tran* BitCoin::getTranTree()
{
    return tranRoot;
}

void BitCoin::deleteTransTree(Tran* root)
{
    if (root == NULL) return;
    
    deleteTransTree(root->left);
    deleteTransTree(root->right);
    
    delete root->coinId;
    delete root->tranId;
    //delete root->ownerWalletID;
    delete root;
}
