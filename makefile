all:	BitCoin Wallet HashEntry WalletManager HashManager BitCoinAppWorker AppDriver
		g++ -o bitcoin BitCoin.o Wallet.o HashTableEntry.o HashManager.o WalletManager.o BitCoinAppWorker.o BitCoinAppDriver.o
		
AppDriver:	BitCoinAppDriver.cpp CommonStructs.h BitCoinAppWorker.h
		g++ -c -g BitCoinAppDriver.cpp

BitCoin:	BitCoin.cpp CommonStructs.h BitCoin.h
		g++ -c -g BitCoin.cpp
		
Wallet:	Wallet.cpp Wallet.h BitCoin.h CommonStructs.h
		g++ -c -g Wallet.cpp
		
HashEntry:	HashTableEntry.cpp HashTableEntry.h Wallet.h
		g++ -c -g HashTableEntry.cpp

WalletManager:	WalletManager.cpp WalletManager.h Wallet.h
		g++ -c -g WalletManager.cpp

HashManager:	HashManager.cpp HashManager.h HashTableEntry.h
		g++ -c -g HashManager.cpp

BitCoinAppWorker:	BitCoinAppWorker.cpp BitCoinAppWorker.h HashManager.h WalletManager.h
		g++ -c -g BitCoinAppWorker.cpp

clean:
	rm -f *.o bitcoin

