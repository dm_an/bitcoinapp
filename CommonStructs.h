#ifndef COMMONSTRUCTS_H
#define COMMONSTRUCTS_H

typedef struct transact
{
    char* tranId;
    char* coinId;
    char *toWalletID;
    char* fromWalletID;
    int amount;
    char *ownerWalletID;
    int year;
    char* time;
    struct transact *left;
    struct transact *right;
    struct transact *next;
} Tran;

extern int gBitCoinValue;
extern int gBucketSize;
extern int gBucketCapacity;
extern int gSendTableNums;
extern int gRecTableNums;
extern const int GLOBAL_SIZE_INCR;


#endif /* COMMONSTRUCTS_H */

