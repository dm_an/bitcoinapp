#include <cstdlib>
#include <cstring>
#include "HashManager.h"

HashManager::HashManager() {
    sendTable = new HashTableEntry[gSendTableNums];
    sendNums = 0;
    
    recTable = new HashTableEntry[gRecTableNums];
    recNums = 0;
}

HashManager::~HashManager() {
    delete [] sendTable;
    delete [] recTable;
}

int HashManager::getHashIndex(char* walletId, int numEntries)
{
    // get the index from the sum of the characters in the walletId
    int charSum = 0;
    for (int i = 0; i < strlen(walletId); i++)
    {
        charSum += walletId[i];
    }
    
    return (charSum % numEntries);
}

void HashManager::updateTables(Wallet* sendWallet, Wallet* recWallet)
{
    int hashIndex = getHashIndex(sendWallet->getID(), gSendTableNums);    
    sendTable[hashIndex].pushWallet(sendWallet);
    
    hashIndex = getHashIndex(recWallet->getID(), gRecTableNums);
    recTable[hashIndex].pushWallet(recWallet);
}

TransNode* HashManager::findEarnings(char* id)
{
    int hashIndex = getHashIndex(id, gRecTableNums);
    if (hashIndex >= 0 && hashIndex < gRecTableNums)        
    {
        Wallet* recWallet = recTable[hashIndex].getWallet(id);
        return (recWallet == NULL) ? NULL : recWallet->getReceiveTrans();
    }
}

TransNode* HashManager::findPayments(char* id)
{
    int hashIndex = getHashIndex(id, gSendTableNums);
    if (hashIndex >= 0 && hashIndex < gSendTableNums)        
    {
        Wallet* sendWallet = sendTable[hashIndex].getWallet(id);
        return (sendWallet == NULL) ? NULL : sendWallet->getSendTrans();
    }
}
