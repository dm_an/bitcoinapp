#ifndef BITCOIN_H
#define BITCOIN_H

#include "CommonStructs.h"

class BitCoin {
private:
    char *id;   // bitcoin id
    char *ownerId;  // the owner wallet id
    int amount;     // the amount left in the bitcoin
    int unspentAmount;
    Tran *tranRoot; // the transaction root for this bitcoin
    Tran* currentNode;
    int tranCount;
    
public:
    //BitCoin();
    BitCoin(char* coinId, char* walletId);;
    ~BitCoin();
    char* getId();
    int balanceAmount();
    int getUnspentAmount();
    int getTransCount();
    void updateAmount(int newamount);
    void addAmount(int newamount);
    void registerTran(int balanceAmount, Tran* tran);
    Tran* getTranTree();
    
private:
    void deleteTransTree(Tran* root);
};

#endif /* BITCOIN_H */

